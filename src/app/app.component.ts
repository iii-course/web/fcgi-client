import {Component, OnInit} from '@angular/core';
import {MpstatService} from "./core/services/mpstat/mpstat.service";
import {interval, Observable} from "rxjs";
import {MpstatResponse} from "./core/models/mpstat.model";
import {mergeMap, startWith} from "rxjs/operators";
import {IostatResponse} from "./core/models/iostat.model";
import {IostatService} from "./core/services/iostat/iostat.service";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  public mpstatStatistics$!: Observable<MpstatResponse>;
  public iostatStatistics$!: Observable<IostatResponse>;

  public readonly requestsIntervalInMs = 5000;

  constructor(
    private mpstatService: MpstatService,
    private iostatService: IostatService
  ) { }

  public ngOnInit() {
    this.mpstatStatistics$ = interval(this.requestsIntervalInMs).pipe(
      startWith(0),
      mergeMap(() => this.mpstatService.getMpstat())
    );

    this.iostatStatistics$ = interval(this.requestsIntervalInMs).pipe(
      startWith(0),
      mergeMap(() => this.iostatService.getIostat())
    );
  }

}

import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment} from "../../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private serverUrl = '';

  constructor(private httpClient: HttpClient) {
    this.serverUrl = environment.apiUrl;
  }

  public get<T>(url: string): Observable<T> {
    return this.httpClient.get<T>(this.formUrl(url));
  }

  private formUrl(part: string): string {
    return `${this.serverUrl}${part}`;
  }
}

import { Injectable } from '@angular/core';
import {ApiService} from "../api/api.service";
import {Observable} from "rxjs";
import {iostatUrl} from "../api/urls";
import {IostatResponse} from "../../models/iostat.model";

@Injectable({
  providedIn: 'root'
})
export class IostatService {

  constructor(private apiService: ApiService) { }

  public getIostat(): Observable<IostatResponse> {
    return this.apiService.get<IostatResponse>(iostatUrl);
  }
}

import { TestBed } from '@angular/core/testing';

import { IostatService } from './iostat.service';

describe('IostatService', () => {
  let service: IostatService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(IostatService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

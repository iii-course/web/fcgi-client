import { TestBed } from '@angular/core/testing';

import { MpstatService } from './mpstat.service';

describe('MpstatService', () => {
  let service: MpstatService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MpstatService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

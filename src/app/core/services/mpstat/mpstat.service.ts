import { Injectable } from '@angular/core';
import {ApiService} from "../api/api.service";
import {Observable} from "rxjs";
import {MpstatResponse} from "../../models/mpstat.model";
import {mpstatUrl} from "../api/urls";

@Injectable({
  providedIn: 'root'
})
export class MpstatService {

  constructor(private apiService: ApiService) { }

  public getMpstat(): Observable<MpstatResponse> {
    return this.apiService.get<MpstatResponse>(mpstatUrl);
  }
}

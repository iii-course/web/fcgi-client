export interface Disk {
  disk_device: string,
  tps: number,
  "kB_read/s": number,
  "kB_wrtn/s": number,
  kB_read: number,
  kB_wrtn: number
}

export interface IostatShot {
  "avg-cpu": {
    user: number,
    nice: number,
    system: number,
    iowait: number,
    steal: number,
    idle: number,
  },
  disk: Disk[]
}

export interface IostatResponse {
  sysstat: {
    hosts: [
      {
        nodename: string, // "k-aspire",
        sysname: string, // "Linux",
        release: string, // "5.4.0-91-generic",
        machine: string, // "x86_64",
        "number-of-cpus": number, // 4,
        date: string,
        statistics: IostatShot[]
      }
    ]
  }
}

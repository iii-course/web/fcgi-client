export interface MpstatCpuLoad {
    cpu: string,
    usr: number,
    nice: number,
    sys: number,
    iowait: number,
    irq: number,
    soft: number,
    steal: number,
    guest: number,
    gnice: number,
    idle: number,
}

export interface MpstatShot {
  timestamp: string,
  "cpu-load": MpstatCpuLoad[]
}

export interface MpstatResponse {
  sysstat: {
    hosts: [
      {
          nodename: string, // "k-aspire",
          sysname: string, // "Linux",
          release: string, // "5.4.0-91-generic",
          machine: string, // "x86_64",
          "number-of-cpus":  number, // 4,
          date: string,
          statistics: MpstatShot[]
      }
    ]
  }
}

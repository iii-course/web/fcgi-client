import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IostatTableComponent } from './iostat-table.component';

describe('IostatTableComponent', () => {
  let component: IostatTableComponent;
  let fixture: ComponentFixture<IostatTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IostatTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IostatTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

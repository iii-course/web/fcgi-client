import {Component, Input} from '@angular/core';
import {BehaviorSubject} from "rxjs";
import { IostatShot} from "../../core/models/iostat.model";

interface IostatShotFlattened {
  user: number,
  nice: number,
  system: number,
  iowait: number,
  steal: number,
  idle: number,

  disk_device: string,
  tps: number,
  "kB_read/s": number,
  "kB_wrtn/s": number,
  kB_read: number,
  kB_wrtn: number
}

@Component({
  selector: 'app-iostat-table',
  templateUrl: './iostat-table.component.html',
  styleUrls: ['./iostat-table.component.scss']
})
export class IostatTableComponent {
  @Input() set statistics(shots: IostatShot[]) {
    shots.forEach(shot => {
      shot["disk"].forEach(disk => {
        const currentShots = this.statisticsShots$.value;
        this.statisticsShots$.next([...currentShots, {...shot["avg-cpu"], ...disk}]);
      });
    });
  };

  public statisticsShots$ = new BehaviorSubject<IostatShotFlattened[]>([]);

  public displayedColumns = [
    'user',
    'nice',
    'system',
    'iowait',
    'steal',
    'idle',

    'disk_device',
    'tps',
    'kB_read/s',
    'kB_wrtn/s',
    'kB_read',
    'kB_wrtn'
  ];

  constructor() { }
}

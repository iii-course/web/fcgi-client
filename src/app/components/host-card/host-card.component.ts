import {Component, Input, OnInit} from '@angular/core';

export interface Host {
  nodename: string, // "k-aspire",
  sysname: string, // "Linux",
  release: string, // "5.4.0-91-generic",
  machine: string, // "x86_64",
  "number-of-cpus": number, // 4,
  date: string,
}

@Component({
  selector: 'app-host-card',
  templateUrl: './host-card.component.html',
  styleUrls: ['./host-card.component.scss']
})
export class HostCardComponent {
  @Input() host!: Host;
}

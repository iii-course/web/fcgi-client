import {Component, Input, OnInit} from '@angular/core';
import {MpstatCpuLoad, MpstatShot} from "../../core/models/mpstat.model";
import {BehaviorSubject} from "rxjs";

export interface MpstatShotFlattened {
  timestamp: string,
  cpu: string,
  usr: number,
  nice: number,
  sys: number,
  iowait: number,
  irq: number,
  soft: number,
  steal: number,
  guest: number,
  gnice: number,
  idle: number,
}

@Component({
  selector: 'app-mpstat-table',
  templateUrl: './mpstat-table.component.html',
  styleUrls: ['./mpstat-table.component.scss']
})
export class MpstatTableComponent {
  @Input() set statistics(shots: MpstatShot[]) {
    shots.forEach(shot => {
      shot["cpu-load"].forEach(cpu => {
        const currentShots = this.statisticsShots$.value;
        this.statisticsShots$.next([...currentShots, {timestamp: shot.timestamp, ...cpu}]);
      });
    });
  };

  public statisticsShots$ = new BehaviorSubject<MpstatShotFlattened[]>([]);

  public displayedColumns = [
    'timestamp',
    'cpu', 'nice', 'sys', 'iowait',
    'irq', 'soft', 'steal',
    'guest', 'gnice', 'idle'
  ];

  constructor() { }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MpstatTableComponent } from './mpstat-table.component';

describe('MpstatTableComponent', () => {
  let component: MpstatTableComponent;
  let fixture: ComponentFixture<MpstatTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MpstatTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MpstatTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
